////////////////////////////////////////////////
//Geocoder, map & marker variables
////////////////////////////////////////////////
var geocoder;
var map;
var marker;

var positionChange = {lat: 0, lng: 0};
var newPosition = {lat: 0, lng: 0};
////////////////////////////////////////////////
//user input fields variables
////////////////////////////////////////////////
var user = document.getElementById("user");
var address = document.getElementById("address");
var email = document.getElementById("email");
var phone = document.getElementById("tel");
var website = document.getElementById("website");

////////////////////////////////////////////////
// BUTTON VARIABLES
////////////////////////////////////////////////
var searchButton = document.getElementById("searchBtn");
var myLocationButton = document.getElementById('getAddress');
var clearButton = document.getElementById("clearBtn");
   
////////////////////////////////////////////////
// DIV VARIABLES
////////////////////////////////////////////////
var mapDiv = document.getElementById('map');
var currentDiv = document.getElementById('current');
var containerDiv = document.querySelector('.container');

////////////////////////////////////////////////
//MVC - sort of...
////////////////////////////////////////////////

function Model(name, address, email, tel, website){
    this.name = name;
    this.address = address;
    this.email = email;
    this.tel = tel;
    this.website = website;
}

function Controller(model){
    this.model = model;
}

Controller.prototype = {
    Validate: function () {
        var f = document.getElementsByTagName('form')[0];
        if(f.checkValidity()) {
            
          if(localStorage){                          
                console.log("localStorage is supported.");            
           } else {
                console.log("Local Storage is not supported.");
           }          
            return true;
        } else {
            return false;
        }
    },
    
    storeData: function() {
            localStorage.setItem('user', JSON.stringify(this.model));
        }     
    }

function View(){
    
}

View.prototype = {
    Data: function() {
        if(localStorage.hasOwnProperty('user')){
            clearButton.disabled = false;
            var paras = document.getElementsByTagName('p');
            var jsonString = localStorage.getItem('user');
            paras[0].innerHTML += " " + JSON.parse(jsonString).name;
            paras[1].innerHTML += " " + JSON.parse(jsonString).address;
            paras[2].innerHTML += " " + JSON.parse(jsonString).email;
            paras[3].innerHTML += " " + JSON.parse(jsonString).tel;
            paras[4].innerHTML += " " + JSON.parse(jsonString).website;
        } else {
            console.log("user doesn't exist");
        }        
    },
    Clear: function () {
        var paras = document.getElementsByTagName('p');
        paras[0].innerHTML = "Name: ";
        paras[1].innerHTML = "Address: ";
        paras[2].innerHTML = "Email: ";
        paras[3].innerHTML = "Phone: ";
        paras[4].innerHTML = "Website: ";
    },
    Map: function() {
        loadMap();
        var inputFormDiv = document.querySelector('.inputForm');
        $(inputFormDiv).removeClass('mid');
        console.log("Map loaded");
    }
}

////////////////////////////////////////////////
//Application Start
////////////////////////////////////////////////

// HIDE ELEMENTS
$(mapDiv).hide();
$(currentDiv).hide();

//WATCH FOR INPUT FIELD CHANGES    
onInputChange(user);    
onInputChange(address);
onInputChange(email);
onInputChange(phone);
onInputChange(website);

$(searchButton).click(function() {
    var controller = new Controller(new Model(user.value, address.value, email.value, phone.value, website.value));
    if(controller.Validate()){
        var view = new View();
        controller.storeData();
        console.log("Data stored in localStorage.");
        view.Map();
        view.Clear();
        view.Data();
    }
});  


//Autocomplete for address input field
var autocomplete;
function initialize() {
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('address')),
      { types: ['geocode'] });
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
  });
}

////////////////////////////////////////////////
//AJAX CALLBACK FUNCTION
////////////////////////////////////////////////
function displayLocation(latitude,longitude){
    var request = new XMLHttpRequest();
    
    var method = 'GET';
    var url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='+latitude+','+longitude+'&sensor=true';
    var async = true;

    request.open(method, url, async);

    request.onreadystatechange = function(){
      if(request.readyState == 4 && request.status == 200){
        var data = JSON.parse(request.responseText);
        var address = data.results[0];  
        var fullAddress = document.getElementById("fullAddress");
        fullAddress.value = address.formatted_address;
        document.getElementById("address").value = address.formatted_address;
        currentAddress = address.address_components[1].short_name + " " + address.address_components[0].short_name + " " + address.address_components[4].short_name + " " +
        address.address_components[5].short_name;  
        //put the address in the location input field
        document.getElementById("address").value = currentAddress;
        map.setCenter(address.geometry.location);
        marker = google.maps.Marker({
                position: address.geometry.location, 
                map: map            
            });
          
        marker.setMap(map);
      }
    };
    
    request.send();
}

  //error check for successful callback and options for getCurrentPosition
  var successCallback = function(position){
    var x = position.coords.latitude;
    var y = position.coords.longitude;      
    newPosition.lat = x;
    newPosition.lng = y;
    displayLocation(x,y);
  };

  var errorCallback = function(error){
    var errorMessage = 'Unknown error';
    switch(error.code) {
      case 1:
        errorMessage = 'Permission denied';
        break;
      case 2:
        errorMessage = 'Position unavailable';
        break;
      case 3:
        errorMessage = 'Timeout';
        break;
    }
    document.write(errorMessage);
  };

  var options = {
    enableHighAccuracy: true,
    timeout: 1000,
    maximumAge: 0
  };

////////////////////////////////////////////////
// FUNCTIONS
////////////////////////////////////////////////

function codeAddress() {
    map = new google.maps.Map(mapDiv,{center: positionChange, zoom: 5});
    geocoder = new google.maps.Geocoder();
    //marker = new google.maps.Marker({position: newPosition, map: map});  
    var address = document.getElementById('address').value;
    
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == 'OK') {
                map.setCenter(results[0].geometry.location);
                var marker2 = new google.maps.Marker({
                    map: map,
                    animation: google.maps.Animation.DROP,
                    position: results[0].geometry.location
                });
            //console.log(marker2.getPosition().lat());
            positionChange.lat = marker2.getPosition().lat();
            positionChange.lng = marker2.getPosition().lng();
            displayLocation(positionChange.lat, positionChange.lng);
            //event listener for the draggable marker to get LAT and LNG coords
            markerEventListenerCallback(marker2);
                
            map.setCenter(marker2.position);
            marker2.setMap(map);  
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }            
        
    }) 
    // This event listener calls addMarker() when the map is clicked.
    google.maps.event.addListener(map, 'click', function(e) {
        placeMarker(e.latLng, map);
        newPosition.lat = e.latLng.lat();
        newPosition.lng = e.latLng.lng();
        displayLocation(newPosition.lat, newPosition.lng);
    });
} 

function getLocation(){
    if (navigator.geolocation) {
        loadCurrentAddress();
        var inputFormDiv = document.querySelector('.inputForm');
        $(inputFormDiv).removeClass('mid');
        console.log("Map loaded");
        navigator.geolocation.getCurrentPosition(successCallback,errorCallback,options);
        var view = new View();
        view.Clear();
        view.Data();
    }
}

function markerEventListener(marker)
{
    //event listener for the draggable marker to get LAT and LNG coords
        google.maps.event.addListener(marker, 'dragend', function(evt){
            
            positionChange.lat = evt.latLng.lat().toFixed(3);
            positionChange.lng = evt.latLng.lng().toFixed(3);
        });

        google.maps.event.addListener(marker, 'dragstart', function(evt){
        });
}
function markerEventListenerCallback(marker)
{
    google.maps.event.addListener(marker, 'dragend', function(evt){
        positionChange.lat = evt.latLng.lat().toFixed(3);
        positionChange.lng = evt.latLng.lng().toFixed(3);
        console.log(positionChange.lat);
        console.log(positionChange.lng);
        displayLocation(positionChange.lat, positionChange.lng);
    });

    google.maps.event.addListener(marker, 'dragstart', function(evt){
        
    });
}

function onInputChange(input){
        input.onchange = function(){
        if(input.value == ''){
        } else {
            $(input).addClass('success');
        }
    }
}

function loadCurrentAddress(){    
    map = new google.maps.Map(mapDiv,{center: positionChange, zoom: 5});
    geocoder = new google.maps.Geocoder();
    // This event listener calls addMarker() when the map is clicked.
    google.maps.event.addListener(map, 'click', function(e) {
        placeMarker(e.latLng, map);
        newPosition.lat = e.latLng.lat();
        newPosition.lng = e.latLng.lng();
        displayLocation(newPosition.lat, newPosition.lng);
    });
    $(mapDiv).show();
    $(currentDiv).show(300);
}

function loadMap(){
    $(mapDiv).show();
    codeAddress();    
    $(currentDiv).show(300);
}

function placeMarker(position, map) {
    var marker = new google.maps.Marker({
      position: position,
      map: map
    });  
    map.panTo(position);
}
//Clear storage, paragraphs used for data & enable clear button
function Clear(){
    localStorage.clear();
    var view = new View();
    view.Clear();
    clearButton.disabled = true;
}
//prevent the default behaviour of form submition
$('form').submit(function (e){    
    e.preventDefault();
})
